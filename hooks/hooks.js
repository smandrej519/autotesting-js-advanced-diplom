function jsonReportTest(test, duration, passed) {
  console.log('\n', '-- Отчет по тестам--\n', JSON.stringify(test, ['parent', 'title'], 2), '\n', ` Время:  ${duration}`, '\n', ` Результат: ${passed}`, '\n');
}

function parseText(text) {
  const str = text.trim();
  const str2 = str.toLowerCase();
  const str3 = str2.replace(/[^A-ZА-Яa-zа-я0-9\s]/g, '');
  return str3;
}

function parseNumber(num) {
  const float = num.trim();
  const float1 = float.replace(/[^0-9\.\,]/g, '');
  const float2 = float1.replace(/\,/g, '.');
  const float3 = parseFloat(float2); // .toFixed(2);
  return float3;
}

async function toBeAscending(array) {
  // eslint-disable-next-line prefer-const
  let arrayPrice = [];
  for (let i = 0; i < array.length; i++) {
    const price = hooks.parseNumber(await array[i].getText());
    arrayPrice.push(price);
  }
  for (let i = 0; i < arrayPrice.length; i++) {
    console.log(arrayPrice[i], arrayPrice[i + 1]);
    if (arrayPrice[i + 1]) {
      expect(arrayPrice[i]).toBeLessThanOrEqual(arrayPrice[i + 1]);
    }
  }
}

function getDateOrder(date) {
  const today = new Date();
  const dd = String(today.getDate() + date).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
  const yyyy = today.getFullYear();
  return `${dd}.${mm}.${yyyy}`;
}

module.exports = {
  jsonReportTest, parseText, parseNumber, toBeAscending, getDateOrder,
};
