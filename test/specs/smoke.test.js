const hooks = require('../../hooks/hooks');
const HomePage = require('../pageobjects/home.page');
const Menu = require('../pageobjects/elements.page');
const LoginPage = require('../pageobjects/login.page');
const BasketPage = require('../pageobjects/basket.page');
const ProductPage = require('../pageobjects/productList.page');
const bonusProgramPage = require('../pageobjects/bonusProgram.page');
const MakeOrdertPage = require('../pageobjects/makeOrder.page');

describe('Checking the operation of the navigation menu', () => {
  afterEach(async () => {
    await browser.deleteCookies();
  });
  const numRandom = Math.floor(Math.random() * 100000);
  const login = `Sema${numRandom}`;
  const email = `sema${numRandom}@test.ru`;
  const password = `qwerty${numRandom}`;
  const firstName = `Александр${numRandom}`;
  const lastName = `Семенов${numRandom}`;
  it('Go to the "Menu" page', async () => {
    await HomePage.open();
    await Menu.catalogPage.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Меню');
  });
  it('Go to the "Pizza" page', async () => {
    await HomePage.open();
    await Menu.catalogPage.moveTo(20, 10);
    await Menu.pizzaPage.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Пицца');
  });
  it('Go to the "Desert" page', async () => {
    await HomePage.open();
    await Menu.catalogPage.moveTo(20, 10);
    await Menu.desertPage.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Десерты');
  });
  it('Go to the "Drinks" page', async () => {
    await HomePage.open();
    await Menu.catalogPage.moveTo(20, 10);
    await Menu.drinksPage.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Напитки');
  });
  it('Go to the "Delivery and payment" page', async () => {
    await HomePage.open();
    await Menu.deliveryAndPaymentPage.click();
    await expect(Menu.title).toHaveTextContaining('Доставка И Оплата');
  });
  it('Go to the "Promo" page', async () => {
    await HomePage.open();
    await Menu.promoPage.click();
    await expect(Menu.title).toHaveTextContaining('Акции');
  });
  it('Go to the "About us pages" page', async () => {
    await HomePage.open();
    await Menu.aboutUsPage.click();
    await expect(Menu.title).toHaveTextContaining('О Нас');
  });
  it('Go to the "Basket pages" page', async () => {
    await HomePage.open();
    await Menu.basketPage.click();
    await expect(Menu.title).toHaveTextContaining('Корзина');
  });
  it('Go to the "My Account pages" page', async () => {
    await HomePage.open();
    await Menu.myAccountPage.click();
    await expect(Menu.title).toHaveTextContaining('Мой Аккаунт');
  });
  it('Go to the "Making an order pages" page', async () => {
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    await HomePage.btnBasketCardFirstSlider[4].waitForDisplayed({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.makeOrdertPage.click();
    await expect(Menu.title).toHaveTextContaining('Оформление Заказа');
  });
  it('Go to the "Bonus program pages" page', async () => {
    await HomePage.open();
    await Menu.bonusProgramPage.click();
    await expect(Menu.title).toHaveTextContaining('Бонусная Программа');
  });
  it('Go to the "Home" page', async () => {
    await HomePage.open();
    await Menu.bonusProgramPage.click();
    await expect(Menu.title).toHaveTextContaining('Бонусная Программа');
    await Menu.homePage.click();
    await expect(HomePage.titleOneSlideHomePage).toHaveTextContaining('ПИЦЦА');
  });
  it('Negative checking the form on the my account page - Registration with empty fields', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.btnRegister).click();
    await LoginPage.register('', '', '');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Пожалуйста, введите корректный email.');
  });
  it('Positive checking the form on the my account page - Registration with valid data', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.btnRegister).click();
    await LoginPage.register(login, email, password);
    await expect(LoginPage.messageAboutRegistration).toHaveTextContaining('Регистрация завершена');
  });
  it('Negative checking the form on the my account page - Password recovery with empty fields.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.linkLostPassword).click();
    await LoginPage.resetPassword('');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Введите имя пользователя или почту.');
  });
  it('Positive checking the form on the my account page - Password recovery with valid username.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.linkLostPassword).click();
    await LoginPage.resetPassword(login);
    await expect(LoginPage.messagePasswordReset).toHaveTextContaining('Электронное письмо было отправлено для сброса пароля.');
  });
  it('Positive checking the form on the my account page - Change the data in your personal account.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization(email, password);
    await expect(LoginPage.textHi).toHaveTextContaining(`Привет ${login}`);
    await (await LoginPage.btnAccountDate).click();
    await (await Menu.myAccountPage).scrollIntoView();
    await LoginPage.fieldFirstnameInMyAccount.waitForClickable({ timeout: 2000 });
    await (await LoginPage.fieldFirstnameInMyAccount).setValue(`${firstName}`);
    await (await LoginPage.fieldLastnameInMyAccount).setValue(`${lastName}`);
    await (await LoginPage.btnSaveInMyAccount).click();
    await expect(LoginPage.messageChangePersonalData).toHaveTextContaining('Данные учетной записи успешно изменены.');
  });
  it('Negative checking the form on the make order page', async () => {
    await LoginPage.open();
    await LoginPage.autorization(login, password);
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    const nameProduct = await hooks.parseText(await (await HomePage.titleProductInCardInSlider[4]).getText());
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await (await Menu.makeOrdertPage).click();
    const nameProductInOrder = await hooks.parseText(await (await MakeOrdertPage.nameProduct).getText());
    await expect(nameProductInOrder).toContain(nameProduct);
    await MakeOrdertPage.makeOrder('', '', '', '', '', '', '');
    await expect(MakeOrdertPage.errorFieldFirstName).toHaveTextContaining('Имя для выставления счета обязательное поле.');
    await expect(MakeOrdertPage.errorFieldLastName).toHaveTextContaining('Фамилия для выставления счета обязательное поле.');
    await expect(MakeOrdertPage.errorFieldAddress).toHaveTextContaining('Адрес для выставления счета обязательное поле.');
    await expect(MakeOrdertPage.errorFieldCity).toHaveTextContaining('Город / Населенный пункт для выставления счета обязательное поле.');
    await expect(MakeOrdertPage.errorFieldState).toHaveTextContaining('Область для выставления счета обязательное поле.');
    await expect(MakeOrdertPage.errorFieldPostcode).toHaveTextContaining('Почтовый индекс для выставления счета обязательное поле.');
    await expect(MakeOrdertPage.errorFieldPhone[0]).toHaveTextContaining('неверный номер телефона.');
    await expect(MakeOrdertPage.errorFieldPhone[1]).toHaveTextContaining('Телефон для выставления счета обязательное поле.');
    await expect(MakeOrdertPage.errorFieldNoCheckbox).toHaveTextContaining('Please read and accept the terms and conditions to proceed with your order.');
  });
  it('Apply the GIVEMEHALYAVA promo code, which gives a 10% discount 2 times', async () => {
    await LoginPage.open();
    await LoginPage.autorization(login, password);
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    await HomePage.cardFirstSlider[4].click();
    await (await ProductPage.selectBort).selectByIndex(2);
    await (await ProductPage.buttonBasket).click();
    await expect(ProductPage.messageAddInBasket).toHaveTextContaining('Вы отложили “Пицца «4 в 1»” в свою корзину.');
    const nameProduct = await hooks.parseText(await (await ProductPage.titleProduct).getText());
    await (await ProductPage.buttonMoreDetailed).click();
    const nameProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
    await expect(BasketPage.nameDopProductOnBasket).toHaveTextContaining('Колбасный борт');
    await (await BasketPage.inputCouponOnBasket).setValue('GIVEMEHALYAVA');
    await BasketPage.btnApplyCouponOnBasket.click();
    await browser.pause(2000);
    await expect(BasketPage.titleFieldDiscountOnBasket).toHaveTextContaining('КУПОН: GIVEMEHALYAVA');
    await expect(BasketPage.messageOnPage).toHaveTextContaining('Coupon code applied successfully.');
    await (await BasketPage.btnGoPaymentOnBasket).click();
    await expect(MakeOrdertPage.titlePage).toHaveTextContaining('ОФОРМЛЕНИЕ ЗАКАЗА');
    await MakeOrdertPage.makeOrder('Андрей', 'Семенов', 'ул. Московская д 1', 'Москва', 'Московская', '010101', '89876543210', true);
    await expect(MakeOrdertPage.messageСonfirmationOrder).toHaveTextContaining('Спасибо! Ваш заказ был получен.');
    const nameProductInReport = await hooks.parseText(await (await MakeOrdertPage.nameProductInReport).getText());
    await expect(nameProductInReport).toContain(nameProduct);
    await expect(MakeOrdertPage.addressSendingInReport).toHaveTextContaining(`Андрей Семенов\nул. Московская д 1\nМосква\nМосковская\n010101\n\n89876543210\n\n${email}`);
  });
  it('Negative 1 checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard('', '');
    await expect(bonusProgramPage.message).toHaveTextContaining('Поле \"Имя\" обязательно для заполнения\nПоле \"Телефон\" обязательно для заполнения');
  });
  it('Positive checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard(login, '89876543210');
    browser.acceptAlert();
    await expect(bonusProgramPage.messageApplyCard).toHaveTextContaining('Ваша карта оформлена!');
  });
  it('Checking contact details in the footer of the site', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await expect(Menu.fieldPhone).toHaveTextContaining('Телефон: +7-999-123-12-31');
    await expect(Menu.fieldEmail).toHaveTextContaining('Email: pizza@fatta.ru');
  });
  it('Click on the "Promotions" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkPromoFooter.click();
    await expect(Menu.title).toHaveTextContaining('Акции');
  });
  it('Click on the "Bonus program pages" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkBonusProgramFooter.click();
    await expect(Menu.title).toHaveTextContaining('Бонусная Программа');
  });
  it('Click on the "All products" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkAllProductsFooter.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Все Товары');
  });
  it('Click on the "Home" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkHomeFooter.click();
    await expect(HomePage.titleOneSlideHomePage).toHaveTextContaining('ПИЦЦА');
  });
  it('Click on the "Delivery and payment" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkDeliveryAndPaymentFooter.click();
    await expect(Menu.title).toHaveTextContaining('Доставка И Оплата');
  });
  it('Click on the "Shopping Cart" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkBasketFooter.click();
    await expect(Menu.title).toHaveTextContaining('Корзина');
  });
  it('Click on the "My account', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkMyAccountFooter.click();
    await expect(Menu.title).toHaveTextContaining('Мой Аккаунт');
  });
  it('Click on the "About us', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkAboutUsFooter.click();
    await expect(Menu.title).toHaveTextContaining('О Нас');
  });
  it('Click on the "Checkout', async () => {
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    await HomePage.btnBasketCardFirstSlider[4].waitForDisplayed({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkCheckoutFooter.click();
    await expect(Menu.title).toHaveTextContaining('Оформление Заказа');
  });
  it('Click on the "Registration', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkRegistrationFooter.click();
    await expect(Menu.title).toHaveTextContaining('Регистрация');
  });
  it('Click on the "UP"', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await expect(Menu.logoFooter).toBeDisplayedInViewport();
    await Menu.btnUp.click();
    await HomePage.titleOneSlideHomePage.waitForDisplayed({ timeout: 2000 });
    await expect(HomePage.titleOneSlideHomePage).toBeDisplayedInViewport();
    await expect(HomePage.titleOneSlideHomePage).toHaveTextContaining('ПИЦЦА');
  });
  it('Click on link the "VKontakte', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkVKontakte.click();
    const tabs = browser.getWindowHandles();
    browser.switchToWindow(tabs[2]);
    await expect(browser).toHaveUrlContaining('vk.com/skillbox');
  });
});
