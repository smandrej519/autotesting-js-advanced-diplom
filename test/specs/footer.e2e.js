const Menu = require('../pageobjects/elements.page');
const HomePage = require('../pageobjects/home.page');

describe('Checking the operation of links in the footer of the site', () => {
  afterEach(async () => {
    await browser.deleteCookies();
  });
  it('Checking contact details in the footer of the site', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await expect(Menu.fieldPhone).toHaveTextContaining('Телефон: +7-999-123-12-31');
    await expect(Menu.fieldEmail).toHaveTextContaining('Email: pizza@fatta.ru');
  });
  it('Click on the "Promotions" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkPromoFooter.click();
    await expect(Menu.title).toHaveTextContaining('Акции');
  });
  it('Click on the "Bonus program pages" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkBonusProgramFooter.click();
    await expect(Menu.title).toHaveTextContaining('Бонусная Программа');
  });
  it('Click on the "All products" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkAllProductsFooter.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Все Товары');
  });
  it('Click on the "Home" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkHomeFooter.click();
    await expect(HomePage.titleOneSlideHomePage).toHaveTextContaining('ПИЦЦА');
  });
  it('Click on the "Delivery and payment" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkDeliveryAndPaymentFooter.click();
    await expect(Menu.title).toHaveTextContaining('Доставка И Оплата');
  });
  it('Click on the "Shopping Cart" link', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkBasketFooter.click();
    await expect(Menu.title).toHaveTextContaining('Корзина');
  });
  it('Click on the "My account', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkMyAccountFooter.click();
    await expect(Menu.title).toHaveTextContaining('Мой Аккаунт');
  });
  it('Click on the "About us', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkAboutUsFooter.click();
    await expect(Menu.title).toHaveTextContaining('О Нас');
  });
  it('Click on the "Checkout', async () => {
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    await HomePage.btnBasketCardFirstSlider[4].waitForDisplayed({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkCheckoutFooter.click();
    await expect(Menu.title).toHaveTextContaining('Оформление Заказа');
  });
  it('Click on the "Registration', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkRegistrationFooter.click();
    await expect(Menu.title).toHaveTextContaining('Регистрация');
  });
  it('Click on the "UP"', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await expect(Menu.logoFooter).toBeDisplayedInViewport();
    await Menu.btnUp.click();
    await HomePage.titleOneSlideHomePage.waitForDisplayed({ timeout: 2000 });
    await expect(HomePage.titleOneSlideHomePage).toBeDisplayedInViewport();
    await expect(HomePage.titleOneSlideHomePage).toHaveTextContaining('ПИЦЦА');
  });
  it('Click on link the "VKontakte', async () => {
    await HomePage.open();
    await Menu.logoFooter.scrollIntoView();
    await Menu.linkVKontakte.click();
    const tabs = browser.getWindowHandles();
    browser.switchToWindow(tabs[2]);
    await expect(browser).toHaveUrlContaining('vk.com/skillbox');
  });
});
