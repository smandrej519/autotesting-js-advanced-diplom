const bonusProgramPage = require('../pageobjects/bonusProgram.page');

describe('Checking the functionality of the site on the  Bonus Program page', () => {
  afterEach(async () => {
    await browser.deleteCookies();
  });
  it('Negative 1 checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard('', '');
    await expect(bonusProgramPage.message).toHaveTextContaining('Поле \"Имя\" обязательно для заполнения\nПоле \"Телефон\" обязательно для заполнения');
  });
  it('Negative 2 checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard('', '89876543210');
    await expect(bonusProgramPage.message).toHaveTextContaining('Поле \"Имя\" обязательно для заполнения');
  });
  it('Negative 3 checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard('Aleksandr', '');
    await expect(bonusProgramPage.message).toHaveTextContaining('Поле \"Телефон\" обязательно для заполнения');
  });
  it('Negative 4 checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard('Aleksandr', 'qwerty');
    await expect(bonusProgramPage.message).toHaveTextContaining('Введен неверный формат телефона');
  });
  it('Negative 5 checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard('Aleksandr', '12345');
    await expect(bonusProgramPage.message).toHaveTextContaining('Введен неверный формат телефона');
  });
  it('Negative 6 checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard('Aleksandr', '898765432109876');
    await expect(bonusProgramPage.message).toHaveTextContaining('Введен неверный формат телефона');
  });
  it('Positive checking the form on the Bonus Program page', async () => {
    await bonusProgramPage.open();
    await bonusProgramPage.applyCard('Aleksandr', '89876543210');
    browser.acceptAlert();
    await expect(bonusProgramPage.messageApplyCard).toHaveTextContaining('Ваша карта оформлена!');
  });
});
