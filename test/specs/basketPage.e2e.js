const hooks = require('../../hooks/hooks');
const HomePage = require('../pageobjects/home.page');
const Menu = require('../pageobjects/elements.page');
const BasketPage = require('../pageobjects/basket.page');
const MakeOrdertPage = require('../pageobjects/makeOrder.page');
const LoginPage = require('../pageobjects/login.page');

describe('Checking the functionality of the basket page site', () => {
  afterEach(async () => {
    if (await BasketPage.btnRemoveDiscountOnBasket.isDisplayed()) {
      await (await BasketPage.btnRemoveDiscountOnBasket).click();
      await BasketPage.btnRemoveDiscountOnBasket.waitForDisplayed({ reverse: true });
    }
    if (await BasketPage.deleteItem.isDisplayed()) {
      await (await BasketPage.deleteItem).waitForClickable({ timeout: 2000 });
      await (await BasketPage.deleteItem).click();
      await BasketPage.deleteItem.waitForDisplayed({ reverse: true });
    }
    await browser.deleteCookies();
  });
  it('Put the product in the cart from the main page', async () => {
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    const nameProduct = await hooks.parseText(await (await HomePage.titleProductInCardInSlider[4]).getText());
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.basketPage.click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    const nameProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
    await BasketPage.btnGoPaymentOnBasket.click();
    await expect(MakeOrdertPage.titlePage).toHaveTextContaining('ОФОРМЛЕНИЕ ЗАКАЗА');
    await expect(MakeOrdertPage.messageAskingYouToLog).toHaveTextContaining('Для оформления заказа необходимо авторизоваться.');
    await (await MakeOrdertPage.buttonLogin).click();
    await expect(MakeOrdertPage.messageInFormLogin).toHaveTextContaining('Если вы уже зарегистрированы на сайте, пожалуйста, введите свои данные.');
  });
  it('Increase the number of items in the basket.', async () => {
    const sumProd = '3';
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.basketPage.click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    await (await BasketPage.quantityProductOnBasket).setValue(sumProd);
    await (await BasketPage.btnRefreshOnBasket).click();
    await expect(BasketPage.quantityProductOnBasket).toHaveValue(sumProd);
  });
  it('Remove an item from the Shopping Cart', async () => {
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    const nameProduct = await hooks.parseText(await (await HomePage.titleProductInCardInSlider[4]).getText());
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.basketPage.click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    const nameProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
    await (await BasketPage.deleteItem).click();
    await expect(BasketPage.messageBasketEmpty).toHaveTextContaining('Корзина пуста.');
    await expect(BasketPage.messageOnPage).toHaveTextContaining('удален');
  });
  it('Apply a promo code GIVEMEHALYAVA giving a 10 discount%', async () => {
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    const nameProduct = await hooks.parseText(await (await HomePage.titleProductInCardInSlider[4]).getText());
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.basketPage.click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    const nameProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
    await (await BasketPage.inputCouponOnBasket).setValue('GIVEMEHALYAVA');
    await BasketPage.btnApplyCouponOnBasket.click();
    await BasketPage.fieldAmountOnBasket.waitForDisplayed({ timeout: 2000 });
    const totalPriceProduct = hooks.parseNumber(await BasketPage.fieldTotalOnBasket.getText());
    const discountPriceProduct = hooks.parseNumber(await BasketPage.fieldDiscountOnBasket.getText());
    const amountPriceProduct = hooks.parseNumber(await BasketPage.fieldAmountOnBasket.getText());
    await expect(totalPriceProduct - discountPriceProduct).toEqual(amountPriceProduct);
    await expect(BasketPage.messageOnPage).toHaveTextContaining('Coupon code applied successfully.');
    await expect(BasketPage.titleFieldDiscountOnBasket).toHaveTextContaining('КУПОН: GIVEMEHALYAVA');
    await expect(BasketPage.btnRemoveDiscountOnBasket).toBeDisplayedInViewport();
    await (await BasketPage.btnRemoveDiscountOnBasket).click();
    await BasketPage.btnRemoveDiscountOnBasket.waitForDisplayed({ reverse: true });
  });
  it('Apply the wrong promo code', async () => {
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    const nameProduct = await hooks.parseText(await (await HomePage.titleProductInCardInSlider[4]).getText());
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.basketPage.click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    const nameProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
    await (await BasketPage.inputCouponOnBasket).setValue('DC120');
    await BasketPage.btnApplyCouponOnBasket.click();
    await expect(BasketPage.messageError).toHaveTextContaining('Неверный купон.');
    await expect(BasketPage.titleFieldDiscountOnBasket).not.toBeDisplayed();
  });
  it('Apply the GIVEMEHALYAVA promo code, which gives a 10% discount 2 times', async () => {
    await LoginPage.open();
    await LoginPage.autorization('test1@test.net', 'qwerty!123');
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    const nameProduct = await hooks.parseText(await (await HomePage.titleProductInCardInSlider[4]).getText());
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.basketPage.click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    const nameProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
    await (await BasketPage.inputCouponOnBasket).setValue('GIVEMEHALYAVA');
    await BasketPage.btnApplyCouponOnBasket.click();
    await browser.pause(2000);
    await expect(BasketPage.titleFieldDiscountOnBasket).toHaveTextContaining('КУПОН: GIVEMEHALYAVA');
    await expect(BasketPage.messageOnPage).toHaveTextContaining('Coupon code applied successfully.');
    await (await BasketPage.btnGoPaymentOnBasket).click();
    await expect(MakeOrdertPage.titlePage).toHaveTextContaining('ОФОРМЛЕНИЕ ЗАКАЗА');
    await MakeOrdertPage.makeOrder('Андрей', 'Семенов', 'ул. Московская д 1', 'Москва', 'Московская', '010101', '89876543210', true);
    await expect(MakeOrdertPage.messageСonfirmationOrder).toHaveTextContaining('Спасибо! Ваш заказ был получен.');
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    const nameTwoProduct = await hooks.parseText(await (await HomePage.titleProductInCardInSlider[4]).getText());
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.basketPage.click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    const nameTwoProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameTwoProductInBasket).toEqual(nameTwoProduct);
    await (await BasketPage.inputCouponOnBasket).setValue('GIVEMEHALYAVA');
    await BasketPage.btnApplyCouponOnBasket.click();
    await expect(BasketPage.messageError).toHaveTextContaining('Купон уже применен.');
    await expect(BasketPage.titleFieldDiscountOnBasket).not.toBeDisplayed();
  });
  it('Apply the promo code “GIVEMEHALYAVA” when the server is not working.', async () => {
    const puppeteer = await browser.getPuppeteer();
    const page = (await puppeteer.pages())[0];
    await page.setRequestInterception(true);
    page.on('request', (request) => {
      if (request.url().includes('/?wc-ajax=apply_coupon')) {
        request.response({
          status: 500,
        });
      }
      request.continue();
    });
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    const nameProduct = await hooks.parseText(await (await HomePage.titleProductInCardInSlider[4]).getText());
    await HomePage.btnBasketCardFirstSlider[4].waitForClickable({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.basketPage.click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    const nameProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
    await (await BasketPage.inputCouponOnBasket).setValue('GIVEMEHALYAVA');
    await (await BasketPage.btnApplyCouponOnBasket).click();
    await expect(BasketPage.titleFieldDiscountOnBasket).not.toBeDisplayed();
  });
});
