const Menu = require('../pageobjects/elements.page');
const HomePage = require('../pageobjects/home.page');

describe('Checking the operation of the navigation menu', () => {
  afterEach(async () => {
    await browser.deleteCookies();
  });
  it('Go to the "Menu" page', async () => {
    await HomePage.open();
    await Menu.catalogPage.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Меню');
  });
  it('Go to the "Pizza" page', async () => {
    await HomePage.open();
    await Menu.catalogPage.moveTo(20, 10);
    await Menu.pizzaPage.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Пицца');
  });
  it('Go to the "Desert" page', async () => {
    await HomePage.open();
    await Menu.catalogPage.moveTo(20, 10);
    await Menu.desertPage.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Десерты');
  });
  it('Go to the "Drinks" page', async () => {
    await HomePage.open();
    await Menu.catalogPage.moveTo(20, 10);
    await Menu.drinksPage.click();
    await expect(Menu.titleMenuPage).toHaveTextContaining('Напитки');
  });
  it('Go to the "Delivery and payment" page', async () => {
    await HomePage.open();
    await Menu.deliveryAndPaymentPage.click();
    await expect(Menu.title).toHaveTextContaining('Доставка И Оплата');
  });
  it('Go to the "Promo" page', async () => {
    await HomePage.open();
    await Menu.promoPage.click();
    await expect(Menu.title).toHaveTextContaining('Акции');
  });
  it('Go to the "About us pages" page', async () => {
    await HomePage.open();
    await Menu.aboutUsPage.click();
    await expect(Menu.title).toHaveTextContaining('О Нас');
  });
  it('Go to the "Basket pages" page', async () => {
    await HomePage.open();
    await Menu.basketPage.click();
    await expect(Menu.title).toHaveTextContaining('Корзина');
  });
  it('Go to the "My Account pages" page', async () => {
    await HomePage.open();
    await Menu.myAccountPage.click();
    await expect(Menu.title).toHaveTextContaining('Мой Аккаунт');
  });
  it('Go to the "Making an order pages" page', async () => {
    await HomePage.open();
    await HomePage.cardFirstSlider[4].moveTo();
    await HomePage.btnBasketCardFirstSlider[4].waitForDisplayed({ timeout: 2000 });
    await HomePage.btnBasketCardFirstSlider[4].click();
    await expect(HomePage.titleButtonBasketCardInSlider).toHaveTextContaining('ПОДРОБНЕЕ');
    await Menu.makeOrdertPage.click();
    await expect(Menu.title).toHaveTextContaining('Оформление Заказа');
  });
  it('Go to the "Bonus program pages" page', async () => {
    await HomePage.open();
    await Menu.bonusProgramPage.click();
    await expect(Menu.title).toHaveTextContaining('Бонусная Программа');
  });
  it('Go to the "Home" page', async () => {
    await HomePage.open();
    await Menu.bonusProgramPage.click();
    await expect(Menu.title).toHaveTextContaining('Бонусная Программа');
    await Menu.homePage.click();
    await expect(HomePage.titleOneSlideHomePage).toHaveTextContaining('ПИЦЦА');
  });
});
