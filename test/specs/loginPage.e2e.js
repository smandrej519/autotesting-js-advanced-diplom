const HomePage = require('../pageobjects/home.page');
const Menu = require('../pageobjects/elements.page');
const LoginPage = require('../pageobjects/login.page');

describe('Checking the functionality of the site on the my account page', () => {
  afterEach(async () => {
    await browser.deleteCookies();
  });
  const numRandom = Math.floor(Math.random() * 100000);
  const login = `Sema${numRandom}`;
  const email = `sema${numRandom}@test.ru`;
  const password = `qwerty${numRandom}`;
  const firstName = `Александр${numRandom}`;
  const lastName = `Семенов${numRandom}`;
  it('Negative checking the form on the my account page - Registration with empty fields', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.btnRegister).click();
    await LoginPage.register('', '', '');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Пожалуйста, введите корректный email.');
  });
  it('Negative checking the form on the my account page - Registration without a username', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.btnRegister).click();
    await LoginPage.register('', email, password);
    await expect(LoginPage.errorLocator).toHaveTextContaining('Пожалуйста введите корректное имя пользователя.');
  });
  it('Negative checking the form on the my account page - Registration without a password', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.btnRegister).click();
    await LoginPage.register(login, email, '');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Введите пароль для регистрации.');
  });
  it('Negative checking the form on the my account page - Registration without a Email', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.btnRegister).click();
    await LoginPage.register(login, '', password);
    await expect(LoginPage.errorLocator).toHaveTextContaining('Пожалуйста, введите корректный email.');
  });
  it('Positive checking the form on the my account page - Registration with valid data', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.btnRegister).click();
    await LoginPage.register(login, email, password);
    await expect(LoginPage.messageAboutRegistration).toHaveTextContaining('Регистрация завершена');
  });
  it('Negative checking the form on the my account page - Registration with an existing name.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.btnRegister).click();
    await LoginPage.register(login, 'aleks-ndr-as@skillbox.ru', password);
    await expect(LoginPage.errorLocator).toHaveTextContaining('Учетная запись с таким именем пользователя уже зарегистрирована.');
  });
  it('Positive checking the form on the my account page - log out of your account.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization(login, password);
    await expect(LoginPage.textHi).toHaveTextContaining(`Привет ${login}`);
    await (await LoginPage.btnExit).click();
    await expect(LoginPage.inputUsername).toBeDisplayed();
    await expect(LoginPage.inputPassword).toBeDisplayed();
  });
  it('Negative checking the form on the my account page - autorization with empty fields', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization('', '');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Имя пользователя обязательно.');
  });
  it('Negative checking the form on the my account page - autorization without a username', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization('', password);
    await expect(LoginPage.errorLocator).toHaveTextContaining('Имя пользователя обязательно.');
  });
  it('Negative checking the form on the my account page - autorization without a password', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization(login, '');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Пароль обязателен.');
  });
  it('Negative checking the form on the my account page - Authorization with an invalid username.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization('Andrey-aa', password);
    await expect(LoginPage.errorLocator).toHaveTextContaining('Неизвестное имя пользователя. Попробуйте еще раз или укажите адрес почты.');
  });
  it('Negative checking the form on the my account page - Authorization with an invalid password.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization(login, '123ii456');
    await expect(LoginPage.errorLocator).toHaveTextContaining(`Веденный пароль для пользователя ${login} неверный. Забыли пароль?`);
  });
  it('Negative checking the form on the my account page - Password recovery with empty fields.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.linkLostPassword).click();
    await LoginPage.resetPassword('');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Введите имя пользователя или почту.');
  });
  it('Negative checking the form on the my account page - Password recovery with invalid username.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.linkLostPassword).click();
    await LoginPage.resetPassword('eeesss');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Неверное имя пользователя или почта.');
  });
  it('Negative checking the form on the my account page - Password recovery with invalid email.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.linkLostPassword).click();
    await LoginPage.resetPassword('eess@qwer.rt');
    await expect(LoginPage.errorLocator).toHaveTextContaining('Неверное имя пользователя или почта.');
  });
  it('Positive checking the form on the my account page - Password recovery with valid username.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await (await LoginPage.linkLostPassword).click();
    await LoginPage.resetPassword(login);
    await expect(LoginPage.messagePasswordReset).toHaveTextContaining('Электронное письмо было отправлено для сброса пароля.');
  });
  it('Positive checking the form on the my account page - autorization with valid username.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization(login, password);
    await expect(LoginPage.textHi).toHaveTextContaining(`Привет ${login}`);
  });
  it('Positive checking the form on the my account page - autorization with valid email.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization(email, password);
    await expect(LoginPage.textHi).toHaveTextContaining(`Привет ${login}`);
  });
  it('Positive checking the form on the my account page - Validation of data in the personal account.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization(email, password);
    await expect(LoginPage.textHi).toHaveTextContaining(`Привет ${login}`);
    await (await LoginPage.btnAccountDate).click();
    await (await Menu.myAccountPage).scrollIntoView();
    await expect(LoginPage.fieldEmailInMyAccount).toHaveValueContaining(`${email}`);
    await (await LoginPage.btnMyOrders).click();
    await expect(LoginPage.fieldSubtitleInMyAccount).toHaveTextContaining('ЗАКАЗЫ');
  });
  it('Positive checking the form on the my account page - Change the data in your personal account.', async () => {
    await HomePage.open();
    await (await Menu.myAccountPage).click();
    await LoginPage.autorization(email, password);
    await expect(LoginPage.textHi).toHaveTextContaining(`Привет ${login}`);
    await (await LoginPage.btnAccountDate).click();
    await (await Menu.myAccountPage).scrollIntoView();
    await LoginPage.fieldFirstnameInMyAccount.waitForClickable({ timeout: 2000 });
    await (await LoginPage.fieldFirstnameInMyAccount).setValue(`${firstName}`);
    await (await LoginPage.fieldLastnameInMyAccount).setValue(`${lastName}`);
    await (await LoginPage.btnSaveInMyAccount).click();
    await expect(LoginPage.messageChangePersonalData).toHaveTextContaining('Данные учетной записи успешно изменены.');
  });
});
