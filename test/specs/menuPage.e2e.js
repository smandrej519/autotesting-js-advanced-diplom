const hooks = require('../../hooks/hooks');
const HomePage = require('../pageobjects/home.page');
const Menu = require('../pageobjects/elements.page');
const BasketPage = require('../pageobjects/basket.page');
const Catalog = require('../pageobjects/catalog.page');
const ProductPage = require('../pageobjects/productList.page');

describe('Checking the functionality of the site on the menu page', () => {
  afterEach(async () => {
    await browser.deleteCookies();
  });
  it('Add an item to the cart on the "Desserts" page.', async () => {
    await HomePage.open();
    await (await Menu.catalogPage).moveTo();
    await (await Menu.desertPage).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Десерты');
    await (await Catalog.productInCentralWindow).moveTo();
    const nameProduct = await hooks.parseText(await (await Catalog.titleProductInCentralWindow).getText());
    await (await Catalog.btnProductInCentralWindow).waitForClickable({ timeout: 2000 });
    await (await Catalog.btnProductInCentralWindow).click();
    await expect(Catalog.titleButtonProductInCentralWindow).toHaveTextContaining('ПОДРОБНЕЕ');
    await (await Menu.basketPage).click();
    await expect(BasketPage.titlePage).toHaveTextContaining('Корзина');
    const nameProductInBasket = await hooks.parseText(await (await BasketPage.nameProductOnBasket[0]).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
  });
  it('Checking product filtering by price on the Menu page', async () => {
    const slep = 10;
    const result = '130';
    await HomePage.open();
    await (await Menu.catalogPage).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Меню');
    await (await Catalog.filterPrice).scrollIntoView();

    for (let position = 0; position >= -210;) {
      await Catalog.filterPrice.dragAndDrop({ x: position - slep, y: 0 });
      const currentValue = (await Catalog.maxFilterPrice.getText()).replace(/[^0-9\.\,]/g, '');
      if (currentValue === result) {
        break;
      } else {
        position -= slep;
      }
    }
    await (await Catalog.buttonApply).click();
    await Catalog.priceProductsInCentralWindow.forEach(async item => {
      const price = await item.getText();
      expect(hooks.parseNumber(price)).toBeLessThanOrEqual(130);
    });
  });
  it('Checking the “Product categories” block on the “Menu” page', async () => {
    await HomePage.open();
    await (await Menu.catalogPage).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Меню');
    await (await Catalog.dessertCategory).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Десерты');
    await (await Catalog.catalogCategory).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Каталог');
    await (await Catalog.drinksCategory).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Напитки');
    await (await Catalog.pizzaCategory).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Пицца');
  });
  it('Go to the product viewing page from the “Products” block on the “Menu” page', async () => {
    await HomePage.open();
    await (await Menu.catalogPage).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Меню');
    const nameProduct = await hooks.parseText(await (await Catalog.titleProductInBlockOnRight).getText());
    await (await Catalog.productInBlockOnRight).click();
    const nameProductInBasket = await hooks.parseText(await (await ProductPage.titleProduct).getText());
    await expect(nameProductInBasket).toEqual(nameProduct);
    const nameProduct_2 = await hooks.parseText(await (await ProductPage.relatedProducts).getText());
    await (await ProductPage.relatedProducts).click();
    const nameProductInBasket_2 = await hooks.parseText(await (await ProductPage.titleProduct).getText());
    await expect(nameProductInBasket_2).toEqual(nameProduct_2);
  });
  it('Checking the filtering of products by price on the Menu page', async () => {
    await HomePage.open();
    await (await Menu.catalogPage).click();
    await expect(Catalog.titlePage).toHaveTextContaining('Меню');
    await (await Catalog.selectProduct).selectByIndex(3);
    await (await Catalog.productInCentralWindow).waitForClickable({ timeout: 2000 });
    hooks.toBeAscending(await Catalog.priceProductsInCentralWindow);
  });
});
