const Page = require('./page');
/**
 * sub page containing specific selectors and methods for a specific page
 */
class ProductPage extends Page {
  /**
   * define selectors using getter methods
   */

  // title Product on the product page
  get titleProduct() {
    return $('h1.product_title');
  }

  // select Bort on the product page
  get selectBort() {
    return $('#board_pack');
  }

  // quantity Product on the product page
  get quantityProduct() {
    return $('.quantity .input-text');
  }

  // quantity Product on the product page
  get buttonBasket() {
    return $('.cart button[type="submit"]');
  }

  // quantity Product on the product page
  get buttonReview() {
    return $('#tab-title-reviews a');
  }

  // quantity Product on the product page
  get viewReview() {
    return $('#tab-reviews');
  }

  // quantity Product on the product page
  get messageAddInBasket() {
    return $('.woocommerce-message');
  }

  // quantity Product on the product page
  get buttonMoreDetailed() {
    return $('.woocommerce-message a');
  }

  //  Product In the “Related products” section on the product page
  get relatedProducts() {
    return $('.related li:first-child a.collection_title');
  }
}

module.exports = new ProductPage();
