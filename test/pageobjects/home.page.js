const Page = require('./page');
/**
 * sub page containing specific selectors and methods for a specific page
 */
class HomePage extends Page {
  /**
   * define selectors using getter methods
   */

  // The title of the first slider on the main page
  get titleOneSlideHomePage() {
    return $('#product1 .prod-title');
  }

  // The first card in the first slider on the main page
  get cardFirstSlider() {
    return $$('#product1 .slick-slide');
  }

  // The title product in card in the first slider on the main page
  get titleProductInCardInSlider() {
    return $$('#product1 .slick-slide a h3');
  }

  // Button the first card in the first slider on the main page
  get btnBasketCardFirstSlider() {
    return $$('#product1 .slick-slide a.button');
  }

  // Title Button the first card in the first slider on the main page
  get titleButtonBasketCardInSlider() {
    return $('#product1 .slick-slide a.added_to_cart');
  }

  // The left button at the first slider on the main page of the site
  get btnLeftOnMainPage() {
    return $('#product1 .slick-prev');
  }

  // The right button at the first slider on the main page of the site
  get btnRightOnMainPage() {
    return $('#product1 .slick-next');
  }

  // The title of the second slider on the main page
  get titleSecondSliderHomePage() {
    return $('#product2 .prod-title');
  }

  // The first card in the second slider on the main page
  get cardSecondSlider() {
    return $$('#product2 .slick-slide');
  }

  // The title product in card in the second slider on the main page
  get titleProductInCardInSecondSlider() {
    return $$('#product2 .slick-slide a h3');
  }

  // Button the first card in the second slider on the main page
  get btnBasketCardSecondSlider() {
    return $$('#product2 .slick-slide a.button');
  }

  // The title of the third slider on the main page
  get titleThirdSliderHomePage() {
    return $('.ap-cat-list prod-title');
  }

  // The first card in the third slider on the main page
  get CardThirdSlider() {
    return $$('.ap-cat-list .slick-slide');
  }

  // Button the first card in the third slider on the main page
  get btnBasketCardThirdSlider() {
    return $$('.ap-cat-list .slick-slide a.button');
  }

  // The title product in card in the third slider on the main page
  get titleProductInCardInThirdSlider() {
    return $$('.ap-cat-list .slick-slide a h3');
  }

  /**
   * overwrite specific options to adapt it to page object
   */
  open() {
    return super.open();
  }
}

module.exports = new HomePage();
