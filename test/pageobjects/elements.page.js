const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class ElementsPage extends Page {
  /**
   * define selectors using getter methods
   */
  get flashAlert() {
    return $('#flash');
  }

  // Site navigation menu locators
  // Page title
  get title() {
    return $('#accesspress-breadcrumb span');
  }

  // Title Menu pages
  get titleMenuPage() {
    return $('.accesspress-breadcrumb span');
  }

  // Home pages
  get homePage() {
    return $('#menu-primary-menu > li:nth-of-type(1)');
  }

  // Menu pages
  get catalogPage() {
    return $('#menu-primary-menu > li:nth-of-type(2)');
  }

  // Pizza pages
  get pizzaPage() {
    return $('#menu-primary-menu .sub-menu li:nth-of-type(1)');
  }

  // Desert pages
  get desertPage() {
    return $('#menu-primary-menu .sub-menu li:nth-of-type(2)');
  }

  // Drinks pages
  get drinksPage() {
    return $('#menu-primary-menu .sub-menu li:nth-of-type(3)');
  }

  // Delivery and payment page
  get deliveryAndPaymentPage() {
    return $('#menu-primary-menu > li:nth-of-type(3)');
  }

  // Promo pages
  get promoPage() {
    return $('#menu-primary-menu > li:nth-of-type(4)');
  }

  // About us pages
  get aboutUsPage() {
    return $('#menu-primary-menu > li:nth-of-type(5)');
  }

  // Basket pages
  get basketPage() {
    return $('#menu-primary-menu > li:nth-of-type(6)');
  }

  // My Account pages
  get myAccountPage() {
    return $('#menu-primary-menu > li:nth-of-type(7)');
  }

  // Making an order pages
  get makeOrdertPage() {
    return $('#menu-primary-menu > li:nth-of-type(8)');
  }

  // Bonus program pages
  get bonusProgramPage() {
    return $('#menu-primary-menu > li:nth-of-type(9)');
  }

  // Button login
  get btnLogin() {
    return $('.logout');
  }

  // Footer
  // Up button
  get btnUp() {
    return $('#ak-top');
  }

  // VKontakte social network
  get linkVKontakte() {
    return $('#accesspress_cta_simple-2 .text-5-value:nth-of-type(4) a');
  }

  // Field number Phone
  get fieldPhone() {
    return $('.banner-text .text-5-value:nth-of-type(1)');
  }

  // Field Email
  get fieldEmail() {
    return $('.banner-text .text-5-value:nth-of-type(2)');
  }

  // Footer Logo
  get logoFooter() {
    return $('.site-info');
  }

  // Link "Promotions"
  get linkPromoFooter() {
    return $('#pages-2 .page_item:nth-of-type(1) a');
  }

  // Link "Bonus program"
  get linkBonusProgramFooter() {
    return $('#pages-2 .page_item:nth-of-type(2) a');
  }

  // Link "All products"
  get linkAllProductsFooter() {
    return $('#pages-2 .page_item:nth-of-type(3) a');
  }

  // The "Home" link
  get linkHomeFooter() {
    return $('#pages-2 .page_item:nth-of-type(4) a');
  }

  // Link "Delivery and payment"
  get linkDeliveryAndPaymentFooter() {
    return $('#pages-2 .page_item:nth-of-type(5) a');
  }

  // The "Shopping Cart" link
  get linkBasketFooter() {
    return $('#pages-2 .page_item:nth-of-type(6) a');
  }

  // Link "My account"
  get linkMyAccountFooter() {
    return $('#pages-2 .page_item:nth-of-type(7) a');
  }

  // Link "About us"
  get linkAboutUsFooter() {
    return $('#pages-2 .page_item:nth-of-type(8) a');
  }

  // Link "Checkout"
  get linkCheckoutFooter() {
    return $('#pages-2 .page_item:nth-of-type(9) a');
  }

  // Link "Registration"
  get linkRegistrationFooter() {
    return $('#pages-2 .page_item:nth-of-type(10) a');
  }

  open() {
    return super.open();
  }
}

module.exports = new ElementsPage();
