const Page = require('./page');
/**
 * sub page containing specific selectors and methods for a specific page
 */
class CatalogPage extends Page {
  /**
   * define selectors using getter methods
   */

  // title Page on the page
  get titlePage() {
    return $('.accesspress-breadcrumb span');
  }

  // Select sorting of goods on the page
  get selectProduct() {
    return $('.orderby');
  }

  // The link category dessert on the page
  get dessertCategory() {
    return $('.product-categories li:nth-of-type(1) a');
  }

  // The link category catalog on the page
  get catalogCategory() {
    return $('.product-categories li:nth-of-type(2) a');
  }

  // The link category drinks on the page
  get drinksCategory() {
    return $('.product-categories li:nth-of-type(4) a');
  }

  // The link category pizza on the page
  get pizzaCategory() {
    return $('.product-categories li:nth-of-type(5) a');
  }

  // Filtering by price right slider on the page
  get filterPrice() {
    return $('.price_slider_wrapper .ui-slider-handle:nth-of-type(2)');
  }

  // Filtering by price right slider on the page
  get maxFilterPrice() {
    return $('.price_slider_wrapper .price_label .to');
  }

  // Filtering by price right slider on the page
  get buttonApply() {
    return $('.price_slider_wrapper button[type="submit"]');
  }

  // The block on the right is products on the page
  get productInBlockOnRight() {
    return $('#woocommerce_products-2 li:last-child a');
  }

  // The block on the right is products on the page
  get titleProductInBlockOnRight() {
    return $('#woocommerce_products-2 li:last-child .product-title');
  }

  // The product is in the central window on the page
  get productInCentralWindow() {
    return $('.wc-products li:first-child .collection_title');
  }

  // Title product is in the central window on the page
  get titleProductInCentralWindow() {
    return $('.wc-products li:first-child .collection_title');
  }

  // Title product is in the central window on the page
  get priceProductsInCentralWindow() {
    return $$('.wc-products .price-cart bdi');
  }

  // Button the product is in the central window on the page
  get btnProductInCentralWindow() {
    return $('.wc-products li:first-child .add_to_cart_button');
  }

  // Title button the product is in the central window on the page
  get titleButtonProductInCentralWindow() {
    return $('.wc-products li:first-child .price-cart .wc-forward');
  }

  /**
   * overwrite specific options to adapt it to page object
   */
  open() {
    return super.open('product-category/menu');
  }
}

module.exports = new CatalogPage();
