const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
  /**
   * define selectors using getter methods
   */
  // Authorization
  get inputUsername() {
    return $('#username');
  }

  get inputPassword() {
    return $('#password');
  }

  get btnLogin() {
    return $('button[name="login"]');
  }

  // the button to go to the registration page
  get btnRegister() {
    return $('button.custom-register-button');
  }

  // Message about error (locator)
  get errorLocator() {
    return $('.woocommerce-error li');
  }

  // link to go to the password recovery page
  get linkLostPassword() {
    return $('.lost_password a');
  }

  // Register
  get inputUsernameRegisterLocator() {
    return $('#reg_username');
  }

  get inputEmailRegisterLocator() {
    return $('#reg_email');
  }

  get inputPasswordRegisterLocator() {
    return $('#reg_password');
  }

  get btnRegisterLocator() {
    return $('button[name="register"]');
  }

  // Input Username or Email for Reset Password
  get inputUsernameOrEmailLocator() {
    return $('#user_login');
  }

  get btnResetLocator() {
    return $('.lost_reset_password button[type="submit"]');
  }

  get btnExit() {
    return $('.woocommerce-MyAccount-content p:nth-of-type(1) a');
  }

  get textHi() {
    return $('.woocommerce-MyAccount-content p:nth-of-type(1)');
  }

  // Page My Account - button my date
  get btnAccountDate() {
    return $('.woocommerce-MyAccount-navigation-link--edit-account a');
  }

  // Page My Account - button my orders
  get btnMyOrders() {
    return $('.woocommerce-MyAccount-navigation-link--orders a');
  }

  // Page My Account - field Email
  get fieldEmailInMyAccount() {
    return $('#account_email');
  }

  // Page My Account - field subtitle
  get fieldSubtitleInMyAccount() {
    return $('#primary .post-title');
  }

  // Page My Account - field First name
  get fieldFirstnameInMyAccount() {
    return $('#account_first_name');
  }

  // Page My Account - field Last name
  get fieldLastnameInMyAccount() {
    return $('#account_last_name');
  }

  // Page My Account - button Save
  get btnSaveInMyAccount() {
    return $('.woocommerce-EditAccountForm button[type="submit"]');
  }

  // Page My Account - message about registration
  get messageAboutRegistration() {
    return $('#primary .content-page div');
  }

  // Page My Account - message about password reset
  get messagePasswordReset() {
    return $('#primary .woocommerce-message');
  }

  // Page My Account - message successful change of personal data
  get messageChangePersonalData() {
    return $('.woocommerce-MyAccount-content .woocommerce-message');
  }

  /**
   * a method to encapsule automation code to interact with the page
   * e.g. to login using username and password
   */
  // Authorization
  async autorization(username, password) {
    await this.inputUsername.setValue(username);
    await this.inputPassword.setValue(password);
    await this.btnLogin.click();
  }

  // Register
  async register(username, email, password) {
    await this.inputUsernameRegisterLocator.setValue(username);
    await this.inputEmailRegisterLocator.setValue(email);
    await this.inputPasswordRegisterLocator.setValue(password);
    await this.btnRegisterLocator.click();
  }

  // Reset Password
  async resetPassword(username) {
    await this.inputUsernameOrEmailLocator.setValue(username);
    await this.btnResetLocator.click();
  }

  /**
   * overwrite specific options to adapt it to page object
   */
  open() {
    return super.open('my-account');
  }
}

module.exports = new LoginPage();
