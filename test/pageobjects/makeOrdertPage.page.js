const Page = require('./page');
/**
 * sub page containing specific selectors and methods for a specific page
 */
class MakeOrdertPage extends Page {
  /**
   * define selectors using getter methods
   */

  // title Page on the page
  get titlePage() {
    return $('#primary h2.post-title');
  }

  // a message asking you to log on the page
  get messageAskingYouToLog() {
    return $('.woocommerce');
  }

  // order confirmation message to log on the page
  get messageСonfirmationOrder() {
    return $('#primary .woocommerce p.woocommerce-notice');
  }

  // button Login on the page
  get buttonLogin() {
    return $('.woocommerce .showlogin');
  }

  // a message In Form Login on the page
  get messageInFormLogin() {
    return $('form.woocommerce-form p:nth-of-type(1)');
  }

  // field name on the page
  get fieldName() {
    return $('#billing_first_name');
  }

  // field last name on the page
  get fieldLastName() {
    return $('#billing_last_name');
  }

  // field address on the page
  get fieldAddress() {
    return $('#billing_address_1');
  }

  // field city on the page
  get fieldCity() {
    return $('#billing_city');
  }

  // field state on the page
  get fieldState() {
    return $('#billing_state');
  }

  // field postcode on the page
  get fieldPostCode() {
    return $('#billing_postcode');
  }

  // field phone on the page
  get fieldPhone() {
    return $('#billing_phone');
  }

  // field order date on the page
  get fieldOrderDate() {
    return $('#order_date');
  }

  // field comments on the page
  get fieldComments() {
    return $('#order_comments');
  }

  // field sum order on the page
  get fieldTotal() {
    return $('#order_review .order-total bdi');
  }

  // field radio button method payment on the page
  get radioButtonMethodPayment() {
    return $('#payment_method_cod');
  }

  // The "Rules" checkbox on the page
  get checkboxRules() {
    return $('#terms');
  }

  // The "Place an order" button on the page
  get buttonPlaceOrder() {
    return $('#place_order');
  }

  /**
   * overwrite specific options to adapt it to page object
   */
  open() {
    return super.open('checkout');
  }
}

module.exports = new MakeOrdertPage();
