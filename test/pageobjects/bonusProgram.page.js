const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class BonusProgramPage extends Page {
  /**
   * define selectors using getter methods
   */

  // title Page on the page
  get titlePage() {
    return $('#accesspress-breadcrumb span');
  }

  // name field
  get inputUsername() {
    return $('#bonus_username');
  }

  // phone field
  get inputPhone() {
    return $('#bonus_phone');
  }

  // the apply for a Card button
  get btnApplyCard() {
    return $('#bonus_main button[type="submit"]');
  }

  // Message about error (locator)
  get message() {
    return $('#bonus_content');
  }

  // Message about apply for a Card
  get messageApplyCard() {
    return $('#bonus_main h3');
  }

  /**
   * a method to encapsule automation code to interact with the page
   * e.g. to login using username and password
   */
  // apply for a Card
  async applyCard(username, phone) {
    await this.inputUsername.setValue(username);
    await this.inputPhone.setValue(phone);
    await this.btnApplyCard.click();
  }

  /**
   * overwrite specific options to adapt it to page object
   */
  open() {
    return super.open('bonus');
  }
}

module.exports = new BonusProgramPage();
