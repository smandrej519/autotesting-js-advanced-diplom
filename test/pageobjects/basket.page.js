const Page = require('./page');
/**
 * sub page containing specific selectors and methods for a specific page
 */
class BasketPage extends Page {
  /**
   * define selectors using getter methods
   */

  // title Page on the page
  get titlePage() {
    return $('#accesspress-breadcrumb span');
  }

  // Removing an item from the shopping cart page
  get deleteItem() {
    return $('.content-page .product-remove .remove');
  }

  // Locator product name on the shopping cart page
  get nameProductOnBasket() {
    return $$('.content-page .product-name a');
  }

  // Locator product name on the shopping cart page
  get nameDopProductOnBasket() {
    return $('.content-page .product-name dd');
  }

  // Locator price products on the shopping cart page
  get priceProductOnBasket() {
    return $('.content-page .product-price bdi');
  }

  // Locator quantity products on the shopping cart page
  get quantityProductOnBasket() {
    return $('.content-page .product-quantity input');
  }

  // Locator subtotal products on the shopping cart page
  get subtotalProductOnBasket() {
    return $('.content-page .product-subtotal bdi');
  }

  // Locator of the coupon entry field on the shopping cart page
  get inputCouponOnBasket() {
    return $('#coupon_code');
  }

  // Locator Button "Apply coupon" on the shopping cart page
  get btnApplyCouponOnBasket() {
    return $('.content-page button[name="apply_coupon"]');
  }

  // Locator "Refresh Cart" button on the shopping cart page
  get btnRefreshOnBasket() {
    return $('.content-page button[type="submit"]:nth-child(2)');
  }

  // Locator The "Amount" field on the shopping cart page
  get fieldTotalOnBasket() {
    return $('.content-page .cart-subtotal bdi');
  }

  // Locator Discount field on the shopping cart page
  get titleFieldDiscountOnBasket() {
    return $('.content-page .cart-discount th');
  }

  // Locator Discount field on the shopping cart page
  get fieldDiscountOnBasket() {
    return $('.content-page .cart-discount .amount');
  }

  // Locator button Discount field on the shopping cart page
  get btnRemoveDiscountOnBasket() {
    return $('a.woocommerce-remove-coupon');
  }

  // Locator The "Amount" field on the shopping cart page
  get fieldAmountOnBasket() {
    return $('.content-page .order-total bdi');
  }

  // Locator The "Go to payment" button on the shopping cart page
  get btnGoPaymentOnBasket() {
    return $('a.checkout-button');
  }

  // Locator message Basket is Empty cart page
  get messageBasketEmpty() {
    return $('.woocommerce .cart-empty');
  }

  // Locator message  cart page
  get messageOnPage() {
    return $('.woocommerce .woocommerce-message');
  }

  // Locator message  cart page
  get messageError() {
    return $('.woocommerce .woocommerce-error');
  }

  // Locator message  cart page
  get titleSummary() {
    return $('.cart_totals h2');
  }

  /**
   * overwrite specific options to adapt it to page object
   */
  open() {
    return super.open('basket');
  }
}

module.exports = new BasketPage();
